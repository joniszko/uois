import os

os.environ['CUDA_VISIBLE_DEVICES'] = "0"

import numpy as np

import src.data_augmentation as data_augmentation
import src.segmentation as segmentation


class NetRunner:
    def __init__(self):
        self.dsn_config, self.rrn_config, self.uois3d_config = self._get_config()
        self.checkpoint_dir = './checkpoints/'  # TODO: change this to directory of downloaded models
        self.dsn_filename = self.checkpoint_dir + 'DepthSeedingNetwork_3D_TOD_checkpoint.pth'
        self.rrn_filename = self.checkpoint_dir + 'RRN_OID_checkpoint.pth'
        self.uois3d_config['final_close_morphology'] = 'TableTop_v5' in self.rrn_filename
        self._init_model()

    def _get_config(self):
        # ========== Depth Seeding Network Parameters
        dsn_config = {
            # Sizes
            'feature_dim': 64,  # 32 would be normal
            # Mean Shift parameters (for 3D voting)
            'max_GMS_iters': 10,
            'epsilon': 0.05,  # Connected Components parameter
            'sigma': 0.02,  # Gaussian bandwidth parameter
            'num_seeds': 200,  # Used for MeanShift, but not BlurringMeanShift
            'subsample_factor': 5,
            # Misc
            'min_pixels_thresh': 500,
            'tau': 15.,
        }
        # ========== Region Refinement Network parameters
        rrn_config = {
            # Sizes
            'feature_dim': 64,  # 32 would be normal
            'img_H': 224,
            'img_W': 224,
            # architecture parameters
            'use_coordconv': False,
        }
        # ========== UOIS-Net-3D Parameters
        uois3d_config = {
            # Padding for RGB Refinement Network
            'padding_percentage': 0.25,
            # Open/Close Morphology for IMP (Initial Mask Processing) module
            'use_open_close_morphology': True,
            'open_close_morphology_ksize': 9,
            # Largest Connected Component for IMP module
            'use_largest_connected_component': True,
        }
        return dsn_config, rrn_config, uois3d_config

    def _init_model(self):
        self.model = segmentation.UOISNet3D(self.uois3d_config, self.dsn_filename, self.dsn_config,
                                            self.rrn_filename, self.rrn_config)

    def run(self, rgb_images: list, xyz_images: list) -> list:
        """
        :param rgb_images: RGB images - HxWx3 (480x640x3 by default) float32 arrays
        :param depth_images: XYZ point clouds - HxWx3 (480x640x3  by default) float32 arrays
        :return: Objects binary masks - HxWxN bool arrays
        """
        N = len(rgb_images)
        rgb_imgs = np.zeros((N, 480, 640, 3), dtype=np.float32)
        xyz_imgs = np.zeros((N, 480, 640, 3), dtype=np.float32)

        for i, (rgb_img, xyz_img) in enumerate(zip(rgb_images, xyz_images)):
            rgb_imgs[i] = data_augmentation.standardize_image(rgb_img)
            xyz_imgs[i] = xyz_img

        batch = {
            'rgb': data_augmentation.array_to_tensor(rgb_imgs),
            'xyz': data_augmentation.array_to_tensor(xyz_imgs),
        }
        fg_masks, center_offsets, initial_masks, seg_masks = self.model.run_on_batch(batch)
        seg_masks = seg_masks.cpu().numpy()
        masks = [arr.squeeze() for arr in np.split(seg_masks, 2, axis=0)]
        masks = [np.dstack([m == val for val in np.unique(m)[1:]]) for m in masks]
        return masks


if __name__ == '__main__':
    rgb_images, xyz_images, gt_images = [], [], []
    for i in range(2):
        path = f'/home/mikhael/Data/adaptive-grasping-device/uois/example_images/OSD_image_{i}.npy'
        d = np.load(path, allow_pickle=True, encoding='bytes').item()
        rgb_images.append(d['rgb'])
        xyz_images.append(d['xyz'])
        l = d['label']
        gt_images.append(np.dstack([l == val for val in np.unique(l)[1:]]))

    nr = NetRunner()
    ret = nr.run(rgb_images, xyz_images)

    from agd_bench.evaluator import Evaluator
    ev = Evaluator()
    ev.update_batch(ret, gt_images)
    print('kurwa')
